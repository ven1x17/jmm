package ua.dp.skillsup;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 // array of queues (clusters)
 // each thread by random selects which cluster it participates in
 // thread local of cluster index in array
 // maybe have an array of cluster sizes,

 // -------------------
 // max clusters count = 4 * THREAD_COUNT
 // on each pull - if cluster is empty - add new cluster :
 //      check max cluster count
 //      if already max - throw an exception
 //      else create new cluster
 // return pull() out of random cluster
 */
public class ObjectPool {
    public static final int STARTING_CLUSTERS_AMOUNT = 2;
    public static final int CORES = 8;
    public static final int MAX_CLUSTERS_COUNT = CORES * 4;
    public static final int OBJECTS_IN_NODE = 5;
    private final Random RANDOM = new Random();

    private final ObjectFactory factory;
    private ConcurrentLinkedQueue<PoolEntry>[] clusters;

    //id of cluster this thread is assigned to
    private ThreadLocal<Integer> clusterId;


    public ObjectPool(ObjectFactory factory) {
        this.factory = factory;
        clusters = new ConcurrentLinkedQueue[STARTING_CLUSTERS_AMOUNT];
        for (int i=0; i< STARTING_CLUSTERS_AMOUNT; i++) {
            for (int j = 0; j < OBJECTS_IN_NODE; j++) {
                clusters[i].add(new PoolEntry(i, factory.create()));
            }
        }
    }
    /**
     * takes object from pool, if no object available > wait
     * if object wasn't created yet - create it
     * @return
     */
    public PoolEntry pull() {
        int length = clusters.length;
        int fromCluster = RANDOM.nextInt(length);
        PoolEntry result = null;
        if (clusters[fromCluster].size() == 0) {
            // create
            if (length > MAX_CLUSTERS_COUNT) {
                throw new RuntimeException("Can not add a new node");
            }
            addNewCluster(length);
			result = pull();
        } else {
			result = clusters[fromCluster].poll();
        }

        return result;
    }

    private void addNewCluster(int length) {
		int newLength = length + 1;
		clusters = Arrays.copyOf(clusters, newLength);
		// init
		for (int i = 0; i < OBJECTS_IN_NODE; i++) {
			clusters[newLength - 1].add(new PoolEntry(newLength - 1, factory.create()));
		}
    }

    /**
     * releases the object back into the pool
     * @param entry
     */
    public void push(PoolEntry entry) {
        // will throw an exception on overflow
        clusters[entry.clusterId].add(entry);
    }

    class PoolEntry {
        // id of cluster that this entry should return to
        public Integer clusterId;
        public Object object;

        public PoolEntry(Integer clusterId, Object object) {
            this.clusterId = clusterId;
            this.object = object;
        }
    }
}

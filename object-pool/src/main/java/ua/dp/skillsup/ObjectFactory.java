package ua.dp.skillsup;

public interface ObjectFactory {

    Object create();

}

package lrulist.entity;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by User on 03.03.2015.
 */
public class ListNode {
    private AtomicReference<MapEntry> entry;
    private AtomicReference<ListNode> next;
    private AtomicReference<ListNode> prev;

    public ListNode(MapEntry entry, ListNode next, ListNode prev) {
        this.entry = new AtomicReference<>(entry);
        this.next = new AtomicReference<>(next);
        this.prev = new AtomicReference<>(prev);
    }

    public AtomicReference<MapEntry> getEntry() {
        return entry;
    }

    public void setEntry(AtomicReference<MapEntry> entry) {
        this.entry = entry;
    }

    public AtomicReference<ListNode> getNext() {
        return next;
    }

    public void setNext(AtomicReference<ListNode> next) {
        this.next = next;
    }

    public AtomicReference<ListNode> getPrev() {
        return prev;
    }

    public void setPrev(AtomicReference<ListNode> prev) {
        this.prev = prev;
    }
}

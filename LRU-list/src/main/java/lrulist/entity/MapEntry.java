package lrulist.entity;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by User on 03.03.2015.
 */
public class MapEntry<K, V> {
    private K key;
    private V val;
    private AtomicReference<ListNode> node;

    public MapEntry(K key, V val, ListNode node) {
        this.key = key;
        this.val = val;
        this.node = new AtomicReference<>(node);
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getVal() {
        return val;
    }

    public void setVal(V val) {
        this.val = val;
    }

    public AtomicReference<ListNode> getNode() {
        return node;
    }

    public void setNode(AtomicReference<ListNode> node) {
        this.node = node;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MapEntry mapEntry = (MapEntry) o;

        if (key != null ? !key.equals(mapEntry.key) : mapEntry.key != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return key != null ? key.hashCode() : 0;
    }
}

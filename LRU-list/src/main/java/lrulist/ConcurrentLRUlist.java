package lrulist;

import lrulist.collection.ConcurrentMappedLinkedList;
import lrulist.entity.ListNode;
import lrulist.entity.MapEntry;

import java.util.Iterator;

/**
 * Created by User on 03.03.2015.
 */
public class ConcurrentLRUlist<K, V> implements LRUList<K, V> {

    ConcurrentMappedLinkedList<K, V> list;

    public ConcurrentLRUlist(int capacity) {
        this.list = new ConcurrentMappedLinkedList<K, V>(capacity);
    }

    @Override
    public V get(K key) {
        // get from map
        // remove from list
        // add to the linked list
        MapEntry<K, V> mapEntry = list.getFromMap(key);
        if (mapEntry != null) {
            ListNode listNode = list.moveToTail_offer(mapEntry);
            if (listNode != null) {
                list.evictIfNeeded();
            }
            list.tryCleanUp();
            return mapEntry.getVal();
        } else {
            return null;
        }
    }

    @Override
    public V put(K key, V value) {
        // put to the map
        // put to the list
        // return value
        MapEntry<K, V> kvMapEntry = new MapEntry<>(key, value, null);
        MapEntry<K, V> nullIfAbsent = list.putToMap(kvMapEntry);
        ListNode listNode = null;
        if (nullIfAbsent == null) {
            listNode = list.moveToTail_offer(kvMapEntry);
            list.evictIfNeeded();
            return value;
        }
        return (V) listNode.getEntry().get().getVal();
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        return list.iteratorFromHead();
    }
}

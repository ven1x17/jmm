package lrulist.collection;

import lrulist.LRUList;
import lrulist.entity.ListNode;
import lrulist.entity.MapEntry;

import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by User on 03.03.2015.
 */
public class ConcurrentMappedLinkedList<K, V> {

    ConcurrentMap<K, V> map;

    private final int capacity;
    AtomicInteger currentCacheSize = new AtomicInteger(0);

    AtomicBoolean cleanupInProgress = new AtomicBoolean(false);
    ConcurrentLinkedQueue<ListNode> cleanupQueue = new ConcurrentLinkedQueue();
    private static final int CLEANUP_BATCH_SIZE = 10;

    AtomicReference<ListNode> head = new AtomicReference(null);
    AtomicReference<ListNode> tail = new AtomicReference(null);

    public ConcurrentMappedLinkedList(int capacity) {
        this.capacity = capacity;
        this.map = new ConcurrentMap<K, V>();
    }

    public void evictIfNeeded() {
        if (currentCacheSize.getAndIncrement() > capacity) {
            evictLast();
        }
    }

    private ListNode putToListTail(ListNode node) {
        boolean done = false;
        ListNode localTail = null;
        ListNode newNode = null;

        while (!done) {
            localTail = tail.get();
            newNode = new ListNode(node.getEntry().get(), localTail, null);
            done = tail.compareAndSet(localTail, newNode);
        }

        if (localTail == null) {
            head.compareAndSet(null, newNode);
        } else {
            localTail.getPrev().compareAndSet(null, newNode);
        }
        return node;
    }

    public ListNode moveToTail_offer(MapEntry<K, V> entry) {
        ListNode tailNode = tail.get();
        if (tailNode == null || tailNode.getEntry().get() != entry) {
            ListNode oldNode = entry.getNode().get();
            ListNode newNode = new ListNode(entry, null, null);
            if (entry.getNode().compareAndSet(oldNode, newNode)) {
                putToListTail(newNode);
                if (oldNode != null) {
                    addToCleanupQueue(oldNode);
                }
            }

            return newNode;
        }
        return null;
    }

    public void evictLast() {
        ListNode newHead;
        boolean done = false;
        ListNode oldHead = null;
        while (!done) {
            oldHead = head.get();
            if (oldHead != null) {
                newHead = oldHead.getPrev().get();
                if (newHead != null) {
                    done = head.compareAndSet(oldHead, newHead);
                }
            }
        }
//------------------------------------------------------------------
        // this cleanup is actually bad, clean through the queue instead to avoid linking issues
//		prev.getNext().compareAndSet(oldHead, null);
//------------------------------------------------------------------
        addToCleanupQueue(oldHead);

//        addToCleanupQueue(head.get());
    }

    public void addToCleanupQueue(ListNode node) {
        MapEntry<K, V> mapEntry = node.getEntry().getAndSet(null);
        map.remove(mapEntry.getKey(), mapEntry.getVal());
        cleanupQueue.add(node);
    }

    public void tryCleanUp() {
        if (cleanupInProgress.compareAndSet(false, true)) {
            fixHoles();
            cleanupInProgress.set(false);
        }
    }

    private void fixHoles() {
        Iterator<ListNode> iterator = cleanupQueue.iterator();
        int cleaned = 0;
        while (iterator.hasNext() && cleaned < CLEANUP_BATCH_SIZE) {
            ListNode nextHole = iterator.next();
            ListNode next = nextHole.getNext().get();
            ListNode prev = nextHole.getPrev().get();
            if (next != null) {
                next.getPrev().compareAndSet(nextHole, prev);
                prev.getNext().compareAndSet(nextHole, next);
            }

            iterator.remove();
            cleaned++;
        }
    }

    public MapEntry<K, V> getFromMap(K key) {
        return map.get(key);
    }

    public MapEntry<K, V> putToMap(MapEntry<K, V> entry) {
        return map.putIfAbsent(entry.getKey(), entry);
    }

    public Iterator<LRUList.Entry<K, V>> iteratorFromHead() {
        return new Iterator<LRUList.Entry<K, V>>() {
            ListNode current = head.get();
            private static final int MAX_TRIES = 10;
            private int currentTry = 0;

            @Override
            public boolean hasNext() {
                currentTry = 0;
                while (currentTry++ < MAX_TRIES) {
                    ListNode prev = current.getPrev().get();
                    if (prev != null) {
                        return true;
                    }
                }
                return false;
            }

            @Override
            public LRUList.Entry<K, V> next() {
//------------------------------------------------------------------
                // getting entry only once before instantiating key/val pair makes the adders test fail
//				final MapEntry<K, V> mapEntry = current.getEntry().get();
//------------------------------------------------------------------

                LRUList.Entry<K, V> entry = new LRUList.Entry<K, V>() {
                    @Override
                    public K getKey() {
                        return (K) current.getEntry().get().getKey();
                    }

                    @Override
                    public V getValue() {
                        return (V) current.getEntry().get().getVal();
                    }
                };
                current = current.getPrev().get();
                return entry;
            }
        };
    }

    public Iterator<LRUList.Entry<K, V>> iteratorFromTail() {
        return new Iterator<LRUList.Entry<K, V>>() {
            ListNode current = tail.get();

            @Override
            public boolean hasNext() {
                return current.getNext().get() != null;
            }

            @Override
            public LRUList.Entry<K, V> next() {
                LRUList.Entry<K, V> entry = new LRUList.Entry<K, V>() {
                    @Override
                    public K getKey() {
                        return (K) current.getEntry().get().getKey();
                    }

                    @Override
                    public V getValue() {
                        return (V) current.getEntry().get().getVal();
                    }
                };
                current = current.getNext().get();
                return entry;
            }
        };
    }
}

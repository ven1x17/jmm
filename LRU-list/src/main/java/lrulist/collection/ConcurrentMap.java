package lrulist.collection;

import lrulist.entity.MapEntry;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by User on 03.03.2015.
 */
public class ConcurrentMap<K, V> {

    ConcurrentHashMap<K, MapEntry<K, V>> map = new ConcurrentHashMap<>();

    public MapEntry putIfAbsent(K key, MapEntry<K, V> value) {
        return map.putIfAbsent(key, value);
    }

    public MapEntry get(K key) {
        return map.get(key);
    }

    public Collection<MapEntry<K, V>> entrySet() {
        return map.values();
    }

    /**
     * MUCH slower than lrulist.collection.ConcurrentMap#remove(java.lang.Object, java.lang.Object)
     */
    public MapEntry<K, V> remove(K key) {
        return map.remove(key);
    }

    public boolean remove(K key, V val) {
        return map.remove(key, val);
    }
}

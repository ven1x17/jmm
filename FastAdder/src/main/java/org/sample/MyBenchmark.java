/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.sample;

import fast.FastAdder;
import fast.FastVolatileAdder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Threads;
import org.openjdk.jmh.annotations.Warmup;

@BenchmarkMode(Mode.AverageTime)
@Measurement(iterations = 10)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Thread)
@Warmup(iterations = 10)
@Fork(1)
@Threads(1)
public class MyBenchmark {

    public static final int THREAD_COUNT = 8;
    public static final int ICNREMENT_AMOUNT = 1000000;
    private AtomicInteger atomicInteger;
    private FastAdder fastAdder;
    private FastVolatileAdder fastVolatileAdder;

    private Runnable atomicIntIncrementor = new Runnable() {
        @Override
        public void run() {
            for (int i = 0; i < ICNREMENT_AMOUNT; i++) {
                atomicInteger.getAndIncrement();
//                Thread.yield();
            }
        }
    };

    private Runnable fastAdderIncrementor = new Runnable() {
        @Override
        public void run() {
            for (int i = 0; i < ICNREMENT_AMOUNT; i++) {
                fastAdder.increment();
//                Thread.yield();
            }
        }
    };

    private Runnable fastVolatileAdderIncrementor = new Runnable() {
        @Override
        public void run() {
            for (int i = 0; i < ICNREMENT_AMOUNT; i++) {
                fastVolatileAdder.increment();
                //                Thread.yield();
            }
        }
    };

    @Setup
    public void setup() {
        atomicInteger = new AtomicInteger(0);
        fastAdder = new FastAdder(0);
        fastVolatileAdder = new FastVolatileAdder(0);
    }

    @Benchmark
    public int atomicInt() throws InterruptedException {
        // spawn a lot of threads that will call increment
        // get the result of increments
        List<Thread> list = new ArrayList<Thread>(THREAD_COUNT);
        for (int i=0; i < THREAD_COUNT; i++) {
            list.add(new Thread(atomicIntIncrementor));
        }

        for (int i=0; i < THREAD_COUNT; i++) {
            list.get(i).start();
        }

        for (int i=0; i < THREAD_COUNT; i++) {
            list.get(i).join();
        }
        return atomicInteger.get();
    }

    @Benchmark
    public long fastAdder() throws InterruptedException {
        // spawn a lot of threads that will call increment
        // get the result of increments
        List<Thread> list = new ArrayList<Thread>(THREAD_COUNT);
        for (int i=0; i < THREAD_COUNT; i++) {
            list.add(new Thread(fastAdderIncrementor));
        }

        for (int i=0; i < THREAD_COUNT; i++) {
            list.get(i).start();
        }

        for (int i=0; i < THREAD_COUNT; i++) {
            list.get(i).join();
        }
        return fastAdder.get();
    }

    @Benchmark
    public long fastVolatileAdder() throws InterruptedException {
        // spawn a lot of threads that will call increment
        // get the result of increments
        List<Thread> list = new ArrayList<Thread>(THREAD_COUNT);
        for (int i=0; i < THREAD_COUNT; i++) {
            list.add(new Thread(fastVolatileAdderIncrementor));
        }

        for (int i=0; i < THREAD_COUNT; i++) {
            list.get(i).start();
        }

        for (int i=0; i < THREAD_COUNT; i++) {
            list.get(i).join();
        }
        return fastVolatileAdder.get();
    }
}

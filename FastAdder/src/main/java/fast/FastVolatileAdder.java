package fast;

import java.util.concurrent.ConcurrentHashMap;

public class FastVolatileAdder {

	private final int initialValue;

	ConcurrentHashMap<Long, ThreadLocalVolatileCounter> counters = new ConcurrentHashMap<Long, ThreadLocalVolatileCounter>();
	ThreadLocal<ThreadLocalVolatileCounter> counterThreadLocal = new ThreadLocal<ThreadLocalVolatileCounter>();

	public FastVolatileAdder(int initialValue) {
		this.initialValue = initialValue;
	}

	public void increment() {
		getCounterForCurrentThread().increment();
	}

	public int get() {
		int result = initialValue;

		for (ThreadLocalVolatileCounter counter : counters.values()) {
			result += counter.get();
		}
		return result;
	}

	private ThreadLocalVolatileCounter getCounterForCurrentThread() {
		if (counterThreadLocal.get() == null) {
			// else creating new one
			long id = Thread.currentThread().getId();
			ThreadLocalVolatileCounter localCounter = new ThreadLocalVolatileCounter(id);
			counters.put(id, localCounter);
			counterThreadLocal.set(localCounter);
		}

		return counterThreadLocal.get();
	}

}

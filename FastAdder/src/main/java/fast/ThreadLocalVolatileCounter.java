package fast;

public class ThreadLocalVolatileCounter {

	public final long threadId;
	private volatile int counter;

	public ThreadLocalVolatileCounter(long threadId) {
		this.threadId = threadId;
		counter = 0;
	}

	public void increment() {
		counter++;
	}

	public int get() {
		return counter;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ThreadLocalCounter that = (ThreadLocalCounter) o;

		if (threadId != that.threadId)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return (int) (threadId ^ (threadId >>> 32));
	}

}

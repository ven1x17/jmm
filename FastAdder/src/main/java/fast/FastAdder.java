package fast;

import java.util.concurrent.ConcurrentHashMap;

public class FastAdder {

	private final int initialValue;

	ConcurrentHashMap<Long, ThreadLocalCounter> counters = new ConcurrentHashMap<Long, ThreadLocalCounter>();
	ThreadLocal<ThreadLocalCounter> counterThreadLocal = new ThreadLocal<ThreadLocalCounter>();

	public FastAdder(int initialValue) {
		this.initialValue = initialValue;
	}

	public void increment() {
		getCounterForCurrentThread().increment();
	}

	public int get() {
		int result = initialValue;

		for (ThreadLocalCounter counter : counters.values()) {
			result += counter.get();
		}
		return result;
	}

	private ThreadLocalCounter getCounterForCurrentThread() {
		if (counterThreadLocal.get() == null) {
			// else creating new one
			long id = Thread.currentThread().getId();
			ThreadLocalCounter localCounter = new ThreadLocalCounter(id);
			counters.put(id, localCounter);
			counterThreadLocal.set(localCounter);
		}

		return counterThreadLocal.get();
	}

}

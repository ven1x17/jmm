package fast;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

public class FastAdderOomTest {

	public static final int THREAD_COUNT_OOM = 100000000;

	@Test
	public void testOOM() throws InterruptedException {
		ExecutorService executorService = Executors.newFixedThreadPool(8);
		final FastAdder fastAdder = new FastAdder(0);
		for (int i=0; i < THREAD_COUNT_OOM; i++) {
			executorService.submit(
					new Runnable() {
						@Override
						public void run() {
							fastAdder.increment();
						}
					}
			);
		}
		executorService.shutdown();
		executorService.awaitTermination(5, TimeUnit.MINUTES);

	}

}

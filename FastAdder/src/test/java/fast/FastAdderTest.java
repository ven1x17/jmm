package fast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class FastAdderTest {

	public static final int TEST_REPEAT_COUNT = 20;

	public static final int THREAD_COUNT = 8;
	public static final int INCREMENT_TIMES_PER_THREAD = 10000000;

	@Parameterized.Parameters
	public static List<Object[]> data() {
		return Arrays.asList(new Object[TEST_REPEAT_COUNT][0]);
	}

	@Test
	public void testConcurrentIncrement() throws InterruptedException {
		final FastAdder fastAdder = new FastAdder(0);
		Runnable fastAdderIncrementor = new Incrementor(fastAdder, new CountDownLatch(1));

		List<Thread> list = new ArrayList<Thread>(THREAD_COUNT);
		for (int i=0; i < THREAD_COUNT; i++) {
			list.add(new Thread(fastAdderIncrementor));
		}

		for (int i=0; i < THREAD_COUNT; i++) {
			list.get(i).start();
		}

		for (int i=0; i < THREAD_COUNT; i++) {
			list.get(i).join();
		}

		Assert.assertEquals(THREAD_COUNT * INCREMENT_TIMES_PER_THREAD, fastAdder.get());
	}

	@Test
	public void testGetInTheMiddle() throws InterruptedException {
		final FastAdder fastAdder = new FastAdder(0);
		CountDownLatch latch = new CountDownLatch(1);
		Incrementor fastAdderIncrementor = new Incrementor(fastAdder, latch);

		List<Thread> list = new ArrayList<Thread>(THREAD_COUNT);
		for (int i=0; i < THREAD_COUNT; i++) {
			list.add(new Thread(fastAdderIncrementor));
		}

		for (int i=0; i < THREAD_COUNT; i++) {
			list.get(i).start();
		}

		latch.await();

		int firstGet = fastAdder.get();
		int secondGet = fastAdder.get();

		Assert.assertNotEquals(0, firstGet);
		Assert.assertNotEquals(0, secondGet);
		Assert.assertNotEquals(secondGet, firstGet);
	}

	class Incrementor implements Runnable {

		private final FastAdder fastAdder;
		private final CountDownLatch latch;

		Incrementor(FastAdder fastAdder, CountDownLatch latch) {
			this.fastAdder = fastAdder;
			this.latch = latch;
		}

		@Override
		public void run() {
			boolean firstCountDown = true;
			for (int i = 0; i < INCREMENT_TIMES_PER_THREAD; i++) {
				fastAdder.increment();
				if (firstCountDown) {
					latch.countDown();
					firstCountDown = false;
				}
			}
		}
	}
}
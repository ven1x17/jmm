package ua.dp.skillsup;

import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by User on 18.03.2015.
 */
public class LockFreeQueue<E> implements Queue<E> {

    /** The queued items */
    final Object[] items;

    /* padding */
    volatile int some1;
    volatile int some2;
    volatile int some3;
    volatile int some4;
    volatile int some5;
    volatile int some6;
    volatile int some7;

    volatile AtomicLong takeIndex = new AtomicLong(0);
    long cachedPutIndex;

    // this array doesn't actually work as cache line padding
//    volatile int[] cacheLineArray = new int[7];

    /* padding */
    volatile int some11;
    volatile int some12;
    volatile int some13;
    volatile int some14;
    volatile int some15;
    volatile int some16;
    volatile int some17;

    volatile AtomicLong putIndex = new AtomicLong(0);
    long cachedTakeIndex;

    public LockFreeQueue(int capacity) {
        if (capacity <= 0)
            throw new IllegalArgumentException();

        this.items = new Object[getNextPowerOfTwo(capacity)];
    }

    static int getNextPowerOfTwo(int base) {
        return (int) Math.pow(2, (32 - Integer.numberOfLeadingZeros(base - 1)));
    }

    int getActualTakeIndex() {
//        return takeIndex % items.length;
        return takeIndex.get() == 0 ? 0 : (int) (takeIndex.get() & (items.length - 1));
    }

    int getActualPutIndex() {
//        return putIndex % items.length;
        return putIndex.get() == 0 ? 0 : (int) (putIndex.get() & (items.length - 1));
    }

    /**
     * Inserts the specified element into this queue if it is possible to do
     * so immediately without violating capacity restrictions.
     * When using a capacity-restricted queue, this method is generally
     * preferable to {@link #add}, which can fail to insert an element only
     * by throwing an exception.
     */
    @Override
    public boolean offer(E e) {
        long putIndexLocal = putIndex.get();
        if ((putIndexLocal - cachedTakeIndex) >= items.length) {
            cachedTakeIndex = takeIndex.get();
            if ((putIndexLocal - cachedTakeIndex) >= items.length) {
                return false;
            }
        }

        items[getActualPutIndex()] = e;
        putIndex.lazySet(putIndexLocal + 1);
        return true;
    }

    /**
     * Retrieves and removes the head of this queue,
     * or returns <tt>null</tt> if this queue is empty.
     *
     * @return the head of this queue, or <tt>null</tt> if this queue is empty
     */
    @Override
    public E poll() {
        long takeIndexLocal = takeIndex.get();
        if (takeIndexLocal >= cachedPutIndex) {
            cachedPutIndex = putIndex.get();
            if (takeIndexLocal >= cachedPutIndex) {
                return null;
            }
        }
        E item = this.<E>cast(items[getActualTakeIndex()]);
        takeIndex.lazySet(takeIndexLocal + 1);
        return item;
    }

    @SuppressWarnings("unchecked")
    static <E> E cast(Object item) {
        return (E) item;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(E e) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public E remove() {
        return null;
    }

    @Override
    public E element() {
        return null;
    }

    @Override
    public E peek() {
        return null;
    }
}

package ua.dp.skillsup;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by User on 18.03.2015.
 */
public class QueueTest {

    @Test
    public void queueTest() {
        LockFreeQueue<Integer> queue = new LockFreeQueue(2);
        Assert.assertTrue(queue.offer(1)); // put = 1 / 1
        Assert.assertTrue(queue.offer(2));// put = 2 / 0

        Assert.assertNotNull(queue.poll());// take = 1 / 1

        Assert.assertTrue(queue.offer(3));// put = 3 / 1

        Assert.assertNotNull(queue.poll());// take = 2 / 0
        Assert.assertNotNull(queue.poll());// take = 3 / 1

        Assert.assertTrue(queue.offer(4));// put = 4
        Assert.assertTrue(queue.offer(5));// put = 5
        Assert.assertFalse(queue.offer(6));// put = 6

        Assert.assertNotNull(queue.poll());// take = 4
        Assert.assertNotNull(queue.poll());// take = 5
        Assert.assertNull(queue.poll());// take = 6
    }

    @Test
    public void testGetNextPowerOfTwo() {
        Assert.assertEquals(4, LockFreeQueue.getNextPowerOfTwo(4));
        Assert.assertEquals(64, LockFreeQueue.getNextPowerOfTwo(33));
        Assert.assertEquals(128, LockFreeQueue.getNextPowerOfTwo(67));
        Assert.assertEquals(256, LockFreeQueue.getNextPowerOfTwo(255));
        Assert.assertEquals(512, LockFreeQueue.getNextPowerOfTwo(433));

        Assert.assertEquals(67108864, LockFreeQueue.getNextPowerOfTwo(33554433));
    }

    @Test
    public void testGetActualPutIndex() {
        LockFreeQueue<Integer> integers = new LockFreeQueue<>(4);
        Assert.assertEquals(0, integers.getActualPutIndex());
        integers.putIndex.set(1);
        Assert.assertEquals(1, integers.getActualPutIndex());
        integers.putIndex.set(4);
        Assert.assertEquals(0, integers.getActualPutIndex());
        integers.putIndex.set(5);
        Assert.assertEquals(1, integers.getActualPutIndex());
        integers.putIndex.set(9);
        Assert.assertEquals(1, integers.getActualPutIndex());
    }

}

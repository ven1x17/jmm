package ua.dp.skillsup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class OneProducerManyConsumersQueueTest {

	public static final int MAX = 10_000;
	public static final int CONSUMER_COUNT = 10;

	@Test
	public void queueTest() {
		OneProducerManyConsumersQueue<Integer> queue = new OneProducerManyConsumersQueue(2);
		Assert.assertTrue(queue.offer(1)); // put = 1 / 1
		Assert.assertTrue(queue.offer(2));// put = 2 / 0

		Assert.assertNotNull(queue.poll());// take = 1 / 1

		Assert.assertTrue(queue.offer(3));// put = 3 / 1

		Assert.assertNotNull(queue.poll());// take = 2 / 0
		Assert.assertNotNull(queue.poll());// take = 3 / 1

		Assert.assertTrue(queue.offer(4));// put = 4
		Assert.assertTrue(queue.offer(5));// put = 5
		Assert.assertFalse(queue.offer(6));// put = 6

		Assert.assertNotNull(queue.poll());// take = 4
		Assert.assertNotNull(queue.poll());// take = 5
		assertNull(queue.poll());// take = 6
	}

	@Test
	public void oneProducerManyConsumers() throws ExecutionException, InterruptedException, TimeoutException {
		OneProducerManyConsumersQueue<Integer> integers = new OneProducerManyConsumersQueue<>(MAX / 4);

		ExecutorService executorService = Executors.newFixedThreadPool(8);

		Future producer = executorService.submit(new Producer(integers));

		List<Future<Map<Integer, Integer>>> futures = new ArrayList<>(CONSUMER_COUNT);

		Thread.sleep(CONSUMER_COUNT);

		for (int i = 0; i < CONSUMER_COUNT; i++) {
			Future<Map<Integer, Integer>> future = executorService.submit(new Consumer(integers));
			futures.add(future);
		}
		executorService.shutdown();

		final Map<Integer, Integer> map = new HashMap<Integer, Integer>(MAX);

		assertEquals(MAX, producer.get());

		for (Future<Map<Integer, Integer>> mapFuture : futures) {
			Map<Integer, Integer> integerIntegerMap = mapFuture.get(30, TimeUnit.SECONDS);

			assertNull("Empty queue during poll", integerIntegerMap.get(null));

			for (Map.Entry<Integer, Integer> entry : integerIntegerMap.entrySet()) {
				Assert.assertNull(String.format("Consumers consumed the same entry : %s", entry.getKey()),
						map.putIfAbsent(entry.getKey(), entry.getValue()));
			}
		};

		Assert.assertFalse(map.isEmpty());

		for (Integer entry : map.values()) {
			assertEquals(String.format("One consumer consumed multiple times : %s", entry), 1, entry.intValue());
		}

	}

	class Consumer implements Callable<Map<Integer, Integer>> {

		private int consumeTimes = MAX / CONSUMER_COUNT;
		final Queue<Integer> integers;

		Consumer(Queue<Integer> integers) {
			this.integers = integers;
		}

		@Override
		public Map<Integer, Integer> call() throws Exception {
			Map<Integer, Integer> map = new HashMap<Integer, Integer>(consumeTimes);
			while (0 < --consumeTimes) {
				Integer poll = integers.poll();
				Integer value = map.get(poll);
				if (value == null) {
					map.put(poll, 1);
				} else {
					map.put(poll, value + 1);
				}
				Thread.sleep((CONSUMER_COUNT -1));
			}

			return map;
		}
	}

	class Producer implements Callable {

		final Queue<Integer> integers;
		int counter = 0;

		Producer(Queue<Integer> integers) {
			this.integers = integers;
		}

		@Override
		public Integer call() {
			while (MAX > counter++) {
				Assert.assertTrue("Queue overflow", integers.offer(counter));

				try {
					Thread.sleep(1);
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			return --counter;
		}
	}

}
